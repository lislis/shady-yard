(ns shady-yard.shady
  (:require [cljs.js]
            ["glslCanvas" :as glsl]))


(def default-canvas "#shady-canvas")




(defn get-canvas
  "takes a CSS selector string, returns DOM node"
  [selector]
  (js/document.querySelector selector))


(defn get-sandbox
  "takes CSS selector string, returns glsl Canvas"
  [selector]
  (let [cnv (get-canvas selector)
        sandbox (glsl. cnv)]
    sandbox))

(defn load-frag
  "takes a CSS selector and glsl fragment shader string, returns string"
  [canvas string]
  (let [sandbox (get-sandbox canvas)]
    (.load sandbox string)
    string))

(defn set-uniform
  "takes CSS selector and uniform name and values"
  [selector uniform value]
  (let [sandbox (get-sandbox selector)
                                        ;values (map #(js/parseFloat % 2) value)
        ]
    (.setUniform sandbox uniform value)
    value))


;;
;; precision mediump float; void main() { gl_FragColor = vec4(0.2, 0.2, 0.9, 1.0); }
;;
(defn frag
  "Takes a glsl fragment shader string, returns true"
  [string]
  (load-frag default-canvas string)
  true)

(defn uniform
  "takes a uniform name and value, returns true"
  [uniform value]
  (set-uniform default-canvas uniform value))

(defn foo [] 42)
