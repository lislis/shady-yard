(ns shady-yard.core
  (:require
    ;; evaluate
    [cljs.js :as cljs]
    [shadow.cljs.bootstrap.browser :as boot]

    ;; view
    [re-view.core :as v :refer [defview]]
    [re-view.hiccup.core :refer [element]]

    ;; things to eval and display
    [lark.value-viewer.core :as views]
    [re-db.d :as d]
    [re-db.patterns :as patterns]
    [cells.cell :as cell]

    [clojure.string :as string]
    [shady-yard.shady :as lib]))


(defonce c-state (cljs/empty-state))

(defn eval-str [source cb]
  (cljs/eval-str
    c-state
    source
    "[test]"
    {:eval cljs/js-eval
     :load (partial boot/load c-state)
     :ns   (symbol "shady-yard.user")}
    cb))

(def setup-source ["(lib/frag \"
precision mediump float; \n
uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
uniform vec3 u_color;

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    st.x *= u_resolution.x/u_resolution.y;

    vec3 color = vec3(0.);
    color = vec3(st.x,st.y,abs(sin(u_time)));

    gl_FragColor = vec4(color,1.0);
}
\")"])

(defview show-canvas
  "Shows a canvas with an editable textarea and value-view."
  {:key                (fn [_ source] source)
   :view/initial-state (fn [_ source]
                         {:source source})
   :view/did-mount     (fn [this source]
                         (eval-str source (partial swap! (:view/state this) assoc :result)))}
  [{:keys [view/state]}]
  (let [{:keys [source result]} @state
        {:keys [error value]} result]
    [:.editor
     [:canvas.canvas {:id "shady-canvas" :width 400 :height 400}]
     [:textarea.editor__input
      {:value     (:source @state)
       :style     {:height "90vh" :width "33vw"}
       :on-change #(let [source (.. % -target -value)]
                     (swap! state assoc :source source)
                     (eval-str source (partial swap! state assoc :result)))}]
     [:.editor__output
      (if error (element [:.pa3.bg-washed-red
                          [:.b (ex-message error)]
                          [:div (str (ex-data error))]
                          (pr-str (ex-cause error))
                          ])
          [:div.editor__result (views/format-value value)])]]))

(defview go-canvas-go
  "Root view for the page"
  []
  (if-not (d/get ::eval-state :ready?)
    [:.wrapper
     "Loading..."]
    [:.wrapper
     (map show-canvas setup-source)]))

(defonce _
         (boot/init c-state
                    {:path         "/js/bootstrap"
                     :load-on-init '#{shady-yard.user }}
                    (fn []
                      (d/transact! [[:db/add ::eval-state :ready? true]]))))

(defn render []
  (v/render-to-dom (go-canvas-go) "shady-yard"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Protocol extensions to enable rendering of cells and shapes

(extend-type cells.cell/Cell
  cells.cell/ICellStore
  (put-value! [this value]
    (d/transact! [[:db/add :cells (name this) value]]))
  (get-value [this]
    (d/get :cells (name this)))
  (invalidate! [this]
    (patterns/invalidate! d/*db* :ea_ [:cells (name this)]))
  lark.value-viewer.core/IView
  (view [this] (cells.cell/view this)))

(extend-protocol lark.value-viewer.core/IView
  Var
  (view [this] (@this)))

(extend-protocol cells.cell/IRenderHiccup
  object
  (render-hiccup [this] (re-view.hiccup.core/element this)))
