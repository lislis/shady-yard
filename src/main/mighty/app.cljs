(ns mighty.app
  (:require ["glslCanvas" :as glsl]))

(defn shader-foo []
  (js/console.log "glsl canvas")
  (let [canvas (js/document.createElement "canvas")
        sandbox (glsl. canvas)]
    (.appendChild (js/document.querySelector "body") canvas)
    (.load sandbox "precision mediump float; uniform float u_time; void main() { gl_FragColor = vec4(0.8, 0.8, sin(u_time), 1.0); }")
    (set! (.-width canvas) js/window.innerWidth)
    (set! (.-height canvas) js/window.innerHeight)
    (js/console.log sandbox)))



;; start is called by init and after code reloading finishes
(defn ^:dev/after-load start []
  (js/console.log "start")
  (shader-foo))

(defn ^:export init []
  ;; init is called ONCE when the page loads
  ;; this is called in the index.html and must be exported
  ;; so it is available even in :advanced release builds
  (js/console.log "init")
  (start))

;; this is called before any code is reloaded
(defn ^:dev/before-load stop []
  (js/console.log "stop"))
