# Shady Yard

*A playground for fargment shaders*


A bootstrapped CLJS project to edit fragment shaders in the browser.
Basically my stab at building the [Book of Shaders Editor](http://editor.thebookofshaders.com/) with some CLJS sprinkles.

---


## I cloned and now what

Install all the deps and run it!

``` bash
$ yarn install && yarn run watch
```

Open browser at `localhost:8702` and start editing the shader!

---

## What

Uses [glslCanvas](https://github.com/patriciogonzalezvivo/glslCanvas) for easy rendering to a browser canvas. Which is literally what the [Book of Shaders Editor](http://editor.thebookofshaders.com/) uses.

I basically wanted to have thin wrappers around `sandbox.load()` and `sandbox.setUniform()` but I only managed to get the loading to work...

Note: Seems like glslCanvas is currently only working in Chrome(based browsers)... meh


## Thanks

https://code.thheller.com/blog/shadow-cljs/2017/10/14/bootstrap-support.html
https://github.com/mhuebert/shadow-bootstrap-example <- I basically used this to set up this project. thanks Matt!


----

### Random notes

[CLJS cheatsheet](https://cljs.info/cheatsheet/)
[shadow-cljs](http://shadow-cljs.org/)

https://clojureverse.org/t/guide-on-how-to-use-import-npm-modules-packages-in-clojurescript/2298
https://shadow-cljs.github.io/docs/UsersGuide.html#npm-install
